set background=dark
set autoindent
set expandtab
set tabstop=4
set number
set termguicolors
syntax enable
let g:gruvbox_italic=1
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
set wrap
set linebreak
set scrolloff=2
set ruler
set title
set autoread
set wildmenu
set backspace=indent,eol,start
set hlsearch

call plug#begin('~/.vim/installed-plugins')

Plug 'sheerun/vim-polyglot'

call plug#end()
